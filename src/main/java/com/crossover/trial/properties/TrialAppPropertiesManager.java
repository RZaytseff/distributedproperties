package com.crossover.trial.properties;

import java.io.PrintStream;
import java.util.*;

import com.crossover.trial.properties.reader.ResourceReaderFabrica;
import com.crossover.trial.properties.type.PropertyType;

/**
 * A simple main method to load and print properties. 
 * You should feel free to change this class
 * or to create additional class. 
 * You may add additional methods, but must implement the 
 * AppPropertiesManager API contract.
 * 
 * Note: a default constructor is required
 *
 * @author code test administrator
 * 
 * @update by Roman.Zaytseff
 */
public class TrialAppPropertiesManager implements AppPropertiesManager {
	
	private final Logger logger = new Logger(TrialAppPropertiesManager.class.getName());
	
	private AppProperties appProperties = new TrialAppProperties();

	/** 
	 * read and sort properties from all resources 
	 * 
	 * Note: 
	 * 1.	If a property is defined more than once, 
	 * 		the last source to set that property value �wins� and is considered the final property value
	 * 2.	If a property is defined more than once and last value of the property is undefined/unresolved
	 * 		then the last definition of the property is saved 
	 * 		and the property key will be written to  special key list with missing value 
	 */
    @Override
    public AppProperties loadProps(List<String> propUris) {
    	
		for(String path: propUris) { 
			try {
				Properties p = ResourceReaderFabrica.gather(path);
				
				for (Map.Entry<?, ?> e : p.entrySet()) {
					String k = (String) e.getKey();
					String v = (String) e.getValue();
					if("unresolved".equals(k) || v == null || v.length() == 0) {
						appProperties.putMissingProperties(k);
					} else {			
						appProperties.putProperty(k, v);
					}
				}
			} catch (Exception e) {
				logger.error("Source " + path + " is not found or reading with errors");
				appProperties.setValid(false);
				continue;				
			}
		}
        return appProperties;
    }

    @Override
    public void printProperties(AppProperties props, PrintStream sync) {
		/** print known ordered properties */
		for(Object key : appProperties.getKnownProperties()) {
			String k = appProperties.getKeyScript((String)key);
			String v = "" + appProperties.getProperty(k);
			sync.println(k + ", " + PropertyType.type(k, v) + ", " + v);
		}
    }

    
    @Override
    public void printMissingProperties(AppProperties props, PrintStream sync) {
		/** print missing ordered keys */
		for(Object key : appProperties.getMissingProperties()) {
			String k = (String)key;
			sync.println(k + ", " + PropertyType.type(k, null) + ", value is missing");
		}
    }
}
