package com.crossover.trial.properties.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import com.crossover.trial.properties.Logger;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;


/**
 * Class is assigned to read properties from SYSTEM, CLASSPATH, FILE and URL
 * in depends on format of resource data as .PROPERTIES or .JSON
 * If format is undefined than logging error message and return empty properties map.
 * 
 * @author Roman.Zaytseff
 */
enum FormatReaderFabrica {
	PROPERTIES {
		@Override
		public Properties read(Reader reader) throws IOException {
			Properties p = new Properties();
			p.load(reader);
			return p;
		}
	},
	
	 JSON {
		@Override
		public Properties read(Reader reader) throws IOException, JsonSyntaxException {
			BufferedReader br = new BufferedReader(reader);
			String jsonString = br.lines().reduce("", (s1, s2) -> s1 + s2);
			return new Gson().fromJson(jsonString, Properties.class); 
		}
	},
	 
	UNDEFINED {
		@Override
		public Properties read(Reader reader) {
			return new Properties();
		}
	};
	
	public abstract Properties read(Reader reader) throws IOException, JsonSyntaxException ;

	static FormatReaderFabrica formatReader(String source) {
		if(source != null) {
			source = source.toLowerCase();
			if(source.endsWith(".properties")) {
				return PROPERTIES;
			} else if(source.endsWith(".json")) {
				return JSON;
			}
		}
		logger.error("Can't recognize format of data in uri = " + source);
		return UNDEFINED;
	}

	static Properties read(URI uri) {
		Properties p = new Properties(); 
		try (Reader reader
					= new InputStreamReader(uri.toURL().openStream(), StandardCharsets.UTF_8)) { 
			
			p = formatReader(uri.toString()).read(reader);
			
		} catch (MalformedURLException e) {
			loggerError(uri);
		} catch (UnsupportedOperationException | IOException e) {
			loggerError(uri);
		} 
		return p;
	}
	
	private static Logger logger = new Logger(FormatReaderFabrica.class.getName());
	
	private static void loggerError(URI uri) {
		logger.error("Can't load data from uri = " + uri.getPath());
	}
}
