package com.crossover.trial.properties.reader;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.function.Function;
import com.crossover.trial.properties.Logger;
import com.google.gson.JsonSyntaxException;

/**
 * Factory method is assigned to construct
 *  URI in depends of prefix of resource's string representation 
 * and gather info from particular kind of resource 
 * in depends on prefix of resource's path
 * Supported prefixes are "system:", "file:", "classpath:",  "http(s)://", "www." 
 * and without prefix. In last case is used DEFAULT uri strategy.
 * 
 * @author Roman.Zaytseff
 */
public enum ResourceReaderFabrica {
	
	SYSTEM("system:", ResourceReaderFabrica::defaultURI),
	
	CLASSPATH("classpath:", ResourceReaderFabrica::classPathURI),
			
	FILE("file:", ResourceReaderFabrica::fileURI),
	
	HTTP("http://",   ResourceReaderFabrica::httpURI),
	
	HTTPS("https://", ResourceReaderFabrica::httpURI),
	
	WWW("www.",       ResourceReaderFabrica::httpURI),
	
	DEFAULT("", ResourceReaderFabrica::defaultURI);
	
	protected final String prefix;
	
	String getProtocol() {
		return prefix;
	}
	
	protected final Function<String, URI> uri;
	
	ResourceReaderFabrica(String prefix, Function<String, URI> uri) {
		this.prefix = prefix;
		this.uri = uri;
	}
	
	protected static ResourceReaderFabrica parse(String source) {
		if(source != null) {
			source = source.toLowerCase();
			for( ResourceReaderFabrica fabrica: ResourceReaderFabrica.values()) {
				if(source.startsWith(fabrica.getProtocol())) {
					return fabrica;
				} 
			}
		}
		return DEFAULT;
	}
	
	/**
	 * Method for read properties from any type and format resource
	 * @param source - stringName of resource
	 * @return properties that was red from the resource
	 */
	protected Properties read(String source) {
		
		if(ResourceReaderFabrica.SYSTEM.equals(parse(source))) {
			return System.getProperties();
		}
		
		if(ResourceReaderFabrica.CLASSPATH.equals(parse(source))) {
			source = source.substring(prefix.length());	
		}
		return FormatReaderFabrica.read(uri.apply(source));
	}

	/**
	 * Static method for read property from any type and format resource
	 * @param source - stringName of resource
	 * @return properties that was red from the resource
	 */
	public static Properties gather(String source) throws IOException, JsonSyntaxException{
		return parse(source).read(source);
	}

	/**
	 * create URI for resources with prefixes "http/s:", "www."
	 */
	static URI httpURI(String source) {
		try {
			return new URI(source);
		} catch (URISyntaxException e) {
			loggerError(source);
			return defaultURI(source);
		}
	}

	/**
	 * create URI for  resources with prefix "file:"
	 */
	static URI fileURI(String source) {
		try {
			return new URI(source);			
		} catch (URISyntaxException e) {
			loggerError(source);
			return defaultURI(source);
		}
	}

	
	/**
	 * create URI for resources with prefix "classpath:"
	 */
	static URI classPathURI(String source) {
		try { 
			return ResourceReaderFabrica.class.getClassLoader().getResource(source).toURI();
		} catch (URISyntaxException e) {
			loggerError(source);
			return defaultURI(source);
		}
	}

	/**
	 * create URI for resources without prefix or unknown protocol
	 * @throws InvalidPathException, java.io.IOError,  SecurityException
	 */
	static URI defaultURI(String source) {
		return Paths.get(source).toUri(); 
	}

	private static Logger logger = new Logger(ResourceReaderFabrica.class.getName());

	private static void loggerError(String uri) {
		logger.error("Can't recognize uri = " + uri 
				+ " -----> So will be used DEFAULT startegy with this file path");
	}
}
