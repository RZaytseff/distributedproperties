package com.crossover.trial.properties;

import java.util.*;

import com.crossover.trial.properties.type.PropertyType;


/**
 * @author code test administrator
 * 
 * It's supposed use the class in monopoly regime
 * So the class is not thread safe
 * 
 * @update by Roman.Zaytseff
 */
public class TrialAppProperties implements AppProperties {
	
	/** is it configuration valid ? */
	protected boolean valid = true;
	
	/** List keys of properties that are missing or reading with fail  */
	protected static Set<String> missing = new TreeSet<String>();
	
	/** natural order sorted TreeMap of known properties */ 
	protected static TreeMap<String, String> propertiesMap = new TreeMap<String, String>();

	/** the original names and they values as are written in source */ 
	protected static HashMap<String, String> propertiesScript = new HashMap<String, String>();
	
    @Override
    public List<String> getMissingProperties() {
    	String[] keys = new String[missing.size()];
    	return Arrays.asList(missing.toArray(keys));
    }

    @Override
    public List<String> getKnownProperties() {
    	String[] keys = new String[propertiesMap.size()];
        return Arrays.asList(propertiesMap.keySet().toArray(keys));
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
	public void setValid(boolean valid) {
		this.valid = valid;
	}

    @Override
    public void clear() {
    	valid = true;
    	missing = new HashSet<String>();
    	propertiesMap = new TreeMap<String, String>();
    }

    @Override
    public Object get(String key) {
    	String value = null;
    	try {
	     	if(key != null && (value = getProperty(key)) != null) {
	    		return PropertyType.value(value);
	    	} else {
	    		return null;
	    	}
    	} catch (Exception e) {
    		return null;
    	}
    }
    
    /**
     * Type safe accessor to property but without type casting
     * So return type is Object to have opportunity return all possible types.
     * (See cast methods in _TypeSafeAccessor class)
     * 
     * @param key - key of property
     * @return value - type safe value of the property
     * 					
     * @throws IllegalArgumentException - If property dosn't exists or unrecognized type
     */
    public Object propertyValue(String key) {
    	String value = null;
    	try {
	    	if((value = getProperty(key)) != null) {
	    		return PropertyType.value(value); 
	    	} else {
	    		throw new IllegalArgumentException("Property with key = " + key + " don't exists or has empty value");
	    	}
    	} catch (Exception e) {
    		throw new IllegalArgumentException("Can't type safe convert for key = " + key + " and value = " + value);
    	}
    }
    
    @Override
    public String getProperty(String key) {
		return propertiesMap.get(normalizedKey(key));
	}
    
   @Override
	public void putProperty(String key, String value) {
		propertiesMap.put(normalizedKey(key), value);
		propertiesScript.put(normalizedKey(key), key);
	}
    
    @Override
	public String getKeyScript(String key) {
        return propertiesScript.get(normalizedKey(key));
    }

    @Override
	public void putMissingProperties(String missingProperties) {
		missing.addAll(Arrays.asList(missingProperties.split(",")));
	}
    
    private static String normalizedKey(String key) {
    	return key.replace("_", ".").toLowerCase();
    }
}

