package com.crossover.trial.properties;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

/**
 * Provides example usage of the API and classes - although candidates can use this
 * Main method to test if their changes will be accepted by the autograder. If your
 * solution is incompatible with this main method, it will be incompatible with the
 * autograder and may cause your solution to be failed.
 * 
 * @author code test administrator
 * 
 * @update by Roman.Zaytseff
 * @data 11.02.2016
 */
public class Main {
    /**
     * Main method useful for your testing, this method is not tested by the grader.
     *
     * @param args - list of URI's: 'output fileName' 'input source-1' 'input source-2' ... etc.
     * @throws URISyntaxException
     * @throws IOException
     */
    public static void main(String[] args) throws URISyntaxException, IOException {
		if(args.length < 2) {
			System.out.println("No sources for consolidate");
    		System.out.println("Usage: java com.crossover.trial.Main "
    				+ "'List of input URIs that consists properties declarations");
			return;
    	}

        List<String> propertySourceUris = Arrays.asList(args);

        /* invoke the property parser and print out properties in alphabetically order */
        AppPropertiesManager m = new TrialAppPropertiesManager();
        AppProperties props = m.loadProps(propertySourceUris);
        PrintStream printStream = System.out;
        m.printProperties(props, printStream);
        m.printMissingProperties(props, printStream);
    }
}
