package com.crossover.trial.properties.type;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import org.joda.money.Money;
import org.joda.time.DateTime;

/**
 * Class is assigned to parse  String representation of some base type of data 
 * 
 * @author Roman.Zaytseff
 */
class BaseType {
	
	/**
	 * Class assign to parse  String representation of date 
	 */
	static class DateType {
		/**
		 * @return java.util.Date class name
		 * 
		 * Note: The name of this method is IMPORTANT!
		 * because it used in lambda expression in PropertyType class 
		 */
		static String getName() {
			return Date.class.getName();
		}
		
		/**
		 * Convert String representation of date to it Date type
		 * Note: The name of this method is IMPORTANT!
		 * because it used in lambda expression in PropertyType class 
		 * 
		 * @param dateString
		 * @return date in Date type 
		 * @throws IllegalArgumentException
		 * 
		 */
		static Date valueOf(String dateString) {
				return new DateTime(dateString).toDate();
		}
	}

//------------------------------------------------------------------------------------
	/**
	 * Class assign to parse various format of URI
	 * 
	 * @author Roman.Zaytseff
	 */
	enum UriType {
		
		URL(java.net.URL.class.getName()) {
			@Override
			public java.net.URL parse(String value) {
				try {
					return new java.net.URL(value);
				} catch (MalformedURLException e) {
					throw new IllegalArgumentException();
				}
			}
		},
		
		URI(java.net.URI.class.getName()) {
			@Override
			public java.net.URI parse(String value) {
				try {
					return new java.net.URI(value);
				} catch (URISyntaxException e) {
					throw new IllegalArgumentException();
				}
			}
		};
		
		private final String className;
	    
		/**
	 	 * get name of class type
	 	 * @return full class name of the class
	 	 */
	    String getName() {
	    	return className;
	    }
	    
	    /**
	     * 
	     * @param value - string representation of object
	     * @return type save object value
	     * @throws IllegalArgumentException
	     */
		abstract Object parse(String value);
	    
		UriType(String className) { 
	    	this.className = className;
	    }
	    
	 	static boolean validate(String value, String regExPattern) { 
	 		return Arrays.asList(RegExType.values()).stream()
	 				.filter((type) -> (type.parse(value) != null))
	 				.count() > 0;
	 	}
	 	
	}

//------------------------------------------------------------------------------------
	/**
	 * Class assign to parse various format of money 
	 */
	static class MoneyType {
		/**
		 * @return Money class name
		 * 
		 * Note: The name of this method is IMPORTANT!
		 * because it used in lambda expression in PropertyType class 
		 */
		static String getName() {
			return Money.class.getName();
		}
		
		/**
		 * Convert String representation of money to it Money class type
		 * Note: The name of this method is IMPORTANT!
		 * because it used in lambda expression in PropertyType class 
		 * 
		 * @param string representation of money 
		 * @return money in Money type 
		 * 
		 */
		static Money valueOf(String money)  {
			return Money.parse(money);
		}
	}

//------------------------------------------------------------------------------------
	/**
	 * Class assign to parse country format 
	 */
	static class CountryType {
	 	/**
	 	 * get name of Locale (Country or country code) class type
	 	 * @return full class name of Locale
	 	 */
	 	static String getName() {
	 		return Locale.class.getName() + "(Country)";
	 	}
	 	
	 	/**
	 	 * define Locale(Country) constant of it's string representation
	 	 * the function is reversion of function 'toString()'
	 	 * @param value
	 	 * @return Locale enum constant
	 	 */
	 	static Locale valueOf(final String country) {
	 		if(country == null) return null;
	 		final String value = country.toUpperCase();
			return  new Locale( 
		 					Arrays.asList(Locale.getISOCountries()).stream()
		 					.filter((countryCode) -> (countryCode.equals(value) 
		 							|| new Locale(countryCode).getDisplayName(Locale.US).toUpperCase().equals(value)
		 							|| new Locale(countryCode).getDisplayName(Locale.FRANCE).toUpperCase().equals(value))
		 							|| new Locale(countryCode).getDisplayName().toUpperCase().equals(value))
		 					.findFirst().get()
					);
		}
	}
	
	
}