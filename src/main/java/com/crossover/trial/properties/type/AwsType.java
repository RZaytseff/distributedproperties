package com.crossover.trial.properties.type;

import java.util.Arrays;

/**
 * Class assign to define 'Amazon regions' types but don't be using for type safe access
 * Stay here as example if you don't want to load AWS SDK
 * 
 * @author Roman.Zaytseff
 */
class AwsType {
	enum AWS {
		// Rgions types
			GLOBAL("-- Global AWS Region --"),
			GovCloud("us-gov-west-1"), 
			US_EAST_1("us-east-1"),
			US_WEST_1("us-west-1"),
			US_WEST_2("us-west-2"),
			EU_WEST_1("eu-west-1"),
			EU_CENTRAL_1("eu-central-1"),
			AP_SOUTHEAST_1("ap-southeast-1"),
			AP_NORTHEAST_1("ap-northeast-1"),
			AP_SOUTHEAST_2("ap-southeast-2"),
			AP_NORTHEAST_2("ap-northeast-2"),
			SA_EAST_1("sa-east-1"),
			CN_NORTH_1("cn-north-1"),
		//------ expended region's sleng enum
			US_Standard(null),
			US_West("us-west-1"),
			EU_Ireland("EU"),
			AP_Singapore("ap-southeast-1"),
			AP_Sydney("ap-southeast-2"),
			AP_Tokyo("ap-northeast-1"),
			SA_SaoPaulo("sa-east-1");
		
		    private final String type;
	
		    AWS(String type) {
		    	this.type = type; 
		    }
		    
		    @Override
		    public String toString() { 
		    	return type; 
		    }
	};	
	
 	/**
 	 * define string representation of AWS type
 	 * @param string representation value of AWS type
 	 * @return type of value of property
 	 */
 	static String type(AWS aws) {
 		return aws.type;
 	}
 	
 	/**
 	 * get full name of AWS(Amazon Weather Service) class type
 	 * @return full class name of AWS
 	 */
 	static String getName() {
 		return "com.amazonaws.regions.Regions"; 
 	}
 	
 	/**
 	 * define AWS enum constant of it string representation
 	 * the function is reverse of function 'toString()'
 	 * @param value
 	 * @return AWS enum constant
	 */

 	static AWS valueOf(String value) {
		return  Arrays.asList(AWS.values()).stream()
				.filter((aws) -> aws.type.equals(value) || aws.name().equals(value))
				.findFirst().get();
	}
}
