package com.crossover.trial.properties.type;

import java.util.Locale;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Arrays;
import java.util.Date;

/**
 * Class assign to parse various user format of date 
 * 
 * @author Roman.Zaytseff
 */
class UserFormatDateType { 
	/** 
	 * example for popular date formats 
	 *  This list provide here as example only
	 *  In real application usually used two or three formats, 
	 *  for example: date, date + time, date + time + timeZome
	 */
	protected static String[] dateFormats = {
			"dd/MM/yyyy",
			"MM dd, yyyy",
			"MMM dd, yyyy",
			"dd-MMM-yyyy",
			"dd-MM-yyyy kk:mm:ss",
			"dd/MM/yyyy kk:mm:ss zzz",
			"dd-MMM-yyyy kk:mm:ss zzz",
			"MMM dd, yyyy kk:mm:ss zzz",
			"E, MMM dd yyyy",
			"E MM dd kk:mm:ss z yyyy",
			"E, MMM dd yyyy kk:mm:ss zzz",
			"EEE, MMM dd yyyy",
			"EEE, MMM dd yyyy HH:mm:ss",
			"EEE MMM dd kk:mm:ss zzz yyyy",	// Thu Sep 28 00:29:00 JST 2000
			"EEE MMM dd HH:mm:ss zzz yyyy",	// Thu Sep 28 24:59:59 JST 2000
			"EEE MMM dd kk:mm:ss z yyyy", 
			"EEE MMM dd HH:mm:ss z yyyy", 
			"EEEE, MMM dd, yyyy HH:mm:ss a",
			"EEEE, MMM dd, yyyy kk:mm:ss a zzz",
			"EEEE, MMM dd, yyyy kk:mm:ss a ZZZ"
	};
	
	/** 
	 * example of most usable locale 
	 *  This list provide here as example only
	 *  In real application usually used one universal or/and default locale only  
	 */
	protected static Locale[] locales = {
			Locale.ROOT,
			Locale.CANADA,
			Locale.US,
			Locale.UK,
			Locale.getDefault()
	};
	
	
	
	/**
	 * @return java.util.Date class name
	 * 
	 * Note: The name of this method is IMPORTANT!
	 * because it used in lambda expression in PropertyType class 
	 */
	static String getName() {
		return Date.class.getName();
	}
	
	static boolean validate(String dateToValidate) {
		return Arrays.asList(dateFormats).parallelStream().filter
				((dateFormat) -> 
					(Arrays.asList(locales).parallelStream().filter
						((locale) ->
								(valueOf(dateToValidate, dateFormat, locale) != null)
						).count() > 0
					)
				).count() > 0;
	}
	
	/**
	 * Convert String representation of date to it Date type
	 * Note: The name of this method is IMPORTANT!
	 * because it used in lambda expression in PropertyType class 
	 * 
	 * @param dateString
	 * @return date in Date type 
	 * 
	 */
	static Date valueOf(String dateString) {
		Date d;
		for(String dateFormat: dateFormats) {
			for (Locale locale : locales)
				try {
					if((d = valueOf(dateString, dateFormat, locale)) != null) {
						return d;
					}
				} catch (Exception e) {
					continue;
				}
		}
		throw new IllegalArgumentException();
	}
	
	/**
	 * Convert String representation of date to Date type
	 * according to desired date format and locale
	 * 
	 * @param dateString - string representation of date
	 * @param dateFromat - desired date format
	 * @param locale - country locale
	 * @return date as Date type 
	 */
	static Date valueOf(String dateString, String dateFromat, Locale locale) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern(dateFromat).withLocale(locale);
		return fmt.parseDateTime(dateString).toDate();
	}
}
