package com.crossover.trial.properties.type;

import java.time.LocalTime;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.Instant;
import java.util.Date;
import java.util.function.Function;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.Region;

import static com.crossover.trial.properties.type.BaseType.*;

/**
 * The class is assigned to define type of property
 * It's include list of known types that could be extended by another demanded types 
 * 
 * The classes that extend the list of known types must include two methods
 * named for example as 'valueOf(String value)' to parse demanded type
 * and return full class name by executing for example method 'getName()'
 * 
 * @param value - value of property
 * @return type of value
 * 
 * @author Roman.Zaytseff
 */
public enum PropertyType  {
	BOOLEAN(Boolean::valueOf,                        Boolean.class.getName()),
	INTEGER(Integer::valueOf,                        Integer.class.getName()),
	LONG      (Long::valueOf,                        Long.class.getName()),
	FLOAT    (Float::valueOf,                        Float.class.getName()),
	DOUBLE  (Double::valueOf,                        Double.class.getName()),
	LOCAL_DATE  (LocalDate::parse,                   LocalDate.class.getName()),
	LOCAL_TIME  (LocalTime::parse,                   LocalTime.class.getName()),
	LOCAL_DATE_TIME(LocalDateTime::parse,            LocalDateTime.class.getName()),
	TIMESTAMPS(Instant::parse,                       Instant.class.getName()),
	ZONED_DATE_TIME(ZonedDateTime::parse,            ZonedDateTime.class.getName()),
	DATE  (DateType::valueOf,                        DateType.getName()),
	DATE_USERFORMAT(UserFormatDateType::valueOf,     UserFormatDateType.getName()),
	/* for define type  of AWS Region  It's enough use following definition,  see AwsType enum. */
//	REGION (AwsType::valueOf,                        AwsType.getName()),
	/* for type safe access It's need to used following two definitions  */
	AWS_REGION (Region::fromValue,                   Region.class.getName()),
	AWS_REGIONS(Regions::valueOf,                    Regions.class.getName()),
	URL(UriType.URL::parse,                          UriType.URL.getName()),
	URL_STRING(RegExType.URL::parse,                 RegExType.URL.getName()),
	URI_STRING(RegExType.URI::parse,                 RegExType.URI.getName()),
	E_MAIL(RegExType.E_MAIL::parse,                  RegExType.E_MAIL.getName()),
	VAT(RegExType.VAT::parse,                        RegExType.VAT.getName()),
	CREDIT_CARD(RegExType.CREDIT_CARD::parse,        RegExType.CREDIT_CARD.getName()),
	NATIONAL_ID(RegExType.NATIONAL_ID::parse,        RegExType.NATIONAL_ID.getName()),
	IPv4(RegExType.IPv4::parse,                      RegExType.IPv4.getName()),
	GUID(RegExType.GUID::parse,                      RegExType.GUID.getName()),
	COUNTRY(CountryType::valueOf,                    CountryType.getName()),
	COUNTRY_NAME(RegExType.COUNTRY::parse,           RegExType.COUNTRY.getName()),
	STATE(RegExType.PROVINCE::parse,                 RegExType.PROVINCE.getName()),
	MONEY(MoneyType::valueOf,                        MoneyType.getName()),
	CURRENCY(RegExType.CURRENCY::parse,              RegExType.CURRENCY.getName()),
	PHONE_NUMBER(RegExType.PHONE_NUMBER::parse,      RegExType.PHONE_NUMBER.getName()),
	STRING    ((s) -> s,                             String.class.getName()),
	UNDEFINED ((s) -> null,                          "Undefined type");
   
	private final Function<String, Object> valueOf;
	
    private final String type;
    
    public String getType() {
    	return type;
    }
    PropertyType(Function<String, Object> valueOf, String type) { 
    	this.valueOf = valueOf; 
    	this.type = type; 
    }
    
    /**
     * The method try parse input value and 
     * cast any possible Exception to runtime IllegalArgumentException
     * 
     * @param value - string representation of data
     * @return type safe representation of data
     * @throws IllegalArgumentException
     */
	public Object parse(String value) {
		try {
			return valueOf.apply(value); 
		} catch (Exception e) {
			throw new IllegalArgumentException("Couldn't parse value = " + value + " to type " + type);
		}
    }
	
   /**
     * try to define type of string representation of value 
     * and convert it to type safe representation of the value 

     * @param value - value of property
     * @return type safe value
     * @throw IllegalArgumentException
     */
	public static Object value(String value) {
    	for(PropertyType type: PropertyType.values()) {
		   	try {
				Object o = type.parse(value);
				if(("" + o).equalsIgnoreCase(value) 
								|| Date.class.equals(o.getClass()) 
								|| ZonedDateTime.class.equals(o.getClass())) {
		    		return o;
				} 
			} catch(Exception e) {
    			continue;
			}
    	}
    	// if no occasion for this value then return null
    	throw new IllegalArgumentException("Can't type safe convert value = " + value); 
    }
    
 	/**
 	 * Define type due to key -> value pair rule
 	 * 
 	 * NOTE:
 	 * 		This simple example stay here as template 
 	 * 		for possible future extensions 
 	 * 		when real application will be developing

 	 * @param key of property
 	 * @param value of property
 	 * @return type of property pair key->value
 	 */
 	public static String type(String key, String value) {
		if(value == null) {
			return keyType(key);
		} else {
			return valueType(value);
		}
	}

 	/**
 	 * Define property type by value and 
 	 * then return class name of this type 

 	 * @param value of property
 	 * @return type of property's value 
     * @throw IllegalArgumentException or may be other runtime Exceptions
     */
 	public static String valueType(String value) {
		if(value == null || value.length() == 0) {
			return UNDEFINED.type;
		}

    	/** check whether value like something from all known types */
		String type = UNDEFINED.type;
    	for(PropertyType pType: PropertyType.values()) {
		   	try {
		   		/* 
		   		 * try to convert from string representation and 
		   		 * compare value after conversion with source string representation
		   		 * exclude Date and ZonedDateTime types 
		   		 * because during convert and reverse convert to string
		   		 * the resulting string representation convert to default time Zone 
		   		 * that may be differs than source TimeZone
		   		 * 
		   		 * This method check null because 
		   		 * it's possible in future that known types list will be extended
		   		 */
				Object o = pType.parse(value);

				if(("" + o).equalsIgnoreCase(value) 
								|| Date.class.equals(o.getClass()) 
								|| ZonedDateTime.class.equals(o.getClass())) {
		    		return pType.getType();
				} 
			} catch(Exception e) {
    			continue;
			}
    	}
    	
	    /** if property not unDefine then check surrounding apostrophes */
    	if(UNDEFINED.type.equals(type)) {
    		if(value.length() > 1 && value.startsWith("\"") && value.endsWith("\"")) {
    			return STRING.type;
    		} else {
    			return UNDEFINED.type;
    		}
    	}
    	
    	return STRING.type;
 	}
 	    
    /**
     * Define type of key 
     * The method is using to attempt define type when print property with missing value
     * The method stay here as :
     * 						- example of possible KEY NAMING RULE 
     * 						- that is may used as template to developing real application 
     * @param key - key of missing property
     * @return assumption about key type
     */
	private static String keyType(String key) {
		if(key == null || key.length() == 0) {
			return UNDEFINED.type;
		}
		
		key = key.trim().replace("_", ".").toLowerCase();

		if(key.contains("aws.region")) {
			return AwsType.getName();
		}

		if(key.contains(".date")) {
			return DATE.type;
		}

		if(key.contains(".guid")) {
			return GUID.type;
		}

		if(key.contains(".country")) {
			return COUNTRY.type;
		}

		if(key.contains(".url")) {
			return URL.type;
		}

		if(key.contains(".uri")) {
			return URI_STRING.type;
		}

		if(key.contains(".e-mail")) {
			return URL.type;
		}
		
		if(key.contains(".vat")) {
			return VAT.type;
		}
		
		if(key.contains(".money") || key.contains(".currency")) {
			return MONEY.type;
		}

		// there's no known occasion for the key
		return UNDEFINED.type;
    }
    
    @Override
    public String toString() { 
    	return type; 
    }

}
