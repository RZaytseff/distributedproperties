package com.crossover.trial.properties.type;

import java.util.Arrays;

interface RegExPattern {
	String PHONE_NUMBER = 
			"^(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*$";
			//"^\\+(?:[0-9]●?){6,14}[0-9]$";  
			//"^\\+(?:[0-9]●?)(\\(\\d\\d\\d\\))?\\s*\\d\\d\\d[-.]\\d\\d\\d\\d";
			//"^((\\+\\d{1,3}(-| )?\\(?\\d\\)?(-| )?\\d{1,3})|(\\(?\\d{2,3}\\)?))(-| )?(\\d{3,4})(-| )?(\\d{4})(( x| ext)\\d{1,5}){0,1}$";
	String COUNTRY = 
			"(?i)\\A((((AFGHANISTAN|ÅLAND ISLANDS|ÅLAND(?:, ÎLES)?+|ALBANIA|ALBANIE|ALGERIA|ALGÉRIE|AMERICAN SAMOA|SAMOA AMÉRICAINES|ANDORRA|ANDORRE|ANGOLA|ANGUILLA|ANTARCTICA|ANTARCTIQUE|ANTIGUA AND BARBUDA|ANTIGUA-ET-BARBUDA|ARGENTINA|ARGENTINE|ARMENIA|ARMÉNIE|ARUBA|AUSTRIA|AZERBAIJAN|BAHAMAS|BAHRAIN|BANGLADESH|BARBADOS|BELARUS|BELGIUM|BELIZE|BENIN|BERMUDA|BHUTAN|BOLIVIA(?:, PLURINATIONAL STATE OF)?+|BONAIRE(?:, SINT EUSTATIUS AND SABA)?+|BOSNIA AND HERZEGOVINA|BOTSWANA|BOUVET ISLAND|BRAZIL|BRITISH INDIAN OCEAN TERRITORY|BRUNEI DARUSSALAM|BULGARIA|BURKINA FASO|BURUNDI|CAMBODIA|CAMEROON|CANADA|CAPE VERDE|CAYMAN ISLANDS|CENTRAL AFRICAN REPUBLIC|CHAD|CHILE|CHINA|CHRISTMAS ISLAND|COCOS \\(KEELING\\) ISLANDS|COLOMBIA|COMOROS|CONGO|CONGO(?:, THE DEMOCRATIC REPUBLIC OF THE)?+|COOK ISLANDS|COSTA RICA|CÔTE D'IVOIRE|CROATIA|CUBA|CURAÇAO|CYPRUS|CZECH REPUBLIC|DENMARK|DJIBOUTI|DOMINICA|DOMINICAN REPUBLIC|ECUADOR|EGYPT|EL SALVADOR|ENGLAND|EQUATORIAL GUINEA|ERITREA|ESTONIA|ETHIOPIA|FALKLAND ISLANDS \\(MALVINAS\\)|FAROE ISLANDS|FIJI|FINLAND|FRANCE|FRENCH GUIANA|FRENCH POLYNESIA|FRENCH SOUTHERN TERRITORIES|GABON|GAMBIA|GEORGIA|GERMANY|GHANA|GIBRALTAR|GREAT BRITAIN|GREECE|GREENLAND|GRENADA|GUADELOUPE|GUAM|GUATEMALA|GUERNSEY|GUINEA|GUINEA-BISSAU|GUYANA|HAITI|HEARD ISLAND AND MCDONALD ISLANDS|HOLY SEE \\(VATICAN CITY STATE\\)|HONDURAS|HONG KONG|HUNGARY|INDIA|INDONESIA|IRAN(?:, ISLAMIC REPUBLIC OF)?+|IRAQ|IRELAND|ISLE OF MAN|ITALY|JAMAICA|JERSEY|JORDAN|KAZAKHSTAN|KENYA|KIRIBATI|KOREA(?:, DEMOCRATIC PEOPLE'S REPUBLIC OF)?+|KUWAIT|KYRGYZSTAN|LAO PEOPLE'S DEMOCRATIC REPUBLIC|LATVIA|LEBANON|LESOTHO|LIBERIA|LIBYA|LIECHTENSTEIN|LITHUANIA|LUXEMBOURG|MACAO|MACEDONIA(?:, THE FORMER YUGOSLAV REPUBLIC OF)?+|MADAGASCAR|MALAWI|MALAYSIA|MALDIVES|MALI|MALTA|MARSHALL ISLANDS|MARTINIQUE|MAURITANIA|MAURITIUS|MAYOTTE|MEXICO|MICRONESIA(?:, FEDERATED STATES OF)?+|MOLDOVA(?:, REPUBLIC OF)?+|MONACO|MONGOLIA|MONTENEGRO|MONTSERRAT|MOROCCO|MOZAMBIQUE|MYANMAR|NAMIBIA|NAURU|NEPAL|NETHERLANDS|NEW CALEDONIA|NICARAGUA|NIGER|NIGERIA|NIUE|NORFOLK ISLAND|NORTHERN MARIANA ISLANDS|OMAN|PAKISTAN|PALAU|PALESTINE(?:, STATE OF)?+|PANAMA|PAPUA NEW GUINEA|PARAGUAY|PERU|PHILIPPINES|PITCAIRN|POLAND|PORTUGAL|PUERTO RICO|QATAR|RÉUNION|ROMANIA|RUSSIAN FEDERATION|RWANDA|SAINT BARTHÉLEMY|SAINT HELENA(?:, ASCENSION AND TRISTAN DA CUNHA)?+|SAINT KITTS AND NEVIS|SAINT LUCIA|SAINT MARTIN \\(FRENCH PART\\)|SAINT PIERRE AND MIQUELON|SAINT VINCENT AND THE GRENADINES|SAMOA|SAN MARINO|SAO TOME AND PRINCIPE|SAUDI ARABIA|SENEGAL|SERBIA|SEYCHELLES|SIERRA LEONE|SINGAPORE|SINT MAARTEN \\(DUTCH PART\\)|SLOVAKIA|SLOVENIA|SOLOMON ISLANDS|SOMALIA|SOUTH AFRICA|SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS|SOUTH SUDAN|SPAIN|SRI LANKA|SUDAN|SURINAME|SVALBARD AND JAN MAYEN|SWAZILAND|SWEDEN|SYRIAN ARAB REPUBLIC|TAIWAN(?:, PROVINCE OF CHINA)?+|TAJIKISTAN|TANZANIA(?:, UNITED REPUBLIC OF)?+|THAILAND|TIMOR-LESTE|TOGO|TOKELAU|TONGA|TRINIDAD AND TOBAGO|TUNISIA|TURKMENISTAN|TURKS AND CAICOS ISLANDS|TUVALU|UGANDA|UKRAINE|UNITED ARAB EMIRATES|UNITED KINGDOM|UNITED STATES|UNITED STATES MINOR OUTLYING ISLANDS|URUGUAY|UZBEKISTAN|VANUATU|VENEZUELA(?:, BOLIVARIAN REPUBLIC OF)?+|VIET NAM|VIRGIN ISLANDS(?:, BRITISH)?+|VIRGIN ISLANDS(?:, U\\.S\\.)?+|WALLIS AND FUTUNA|WESTERN SAHARA|YEMEN|ZAMBIA|ZIMBABWE|A[D-GIMOQ-TWXZ]|B[ABD-JL-OQ-TVWYZ]|C[ACDFGIK-ORU-Z]|D[EJKMOZ]|E[CEGHRST]|F[IJKMOR]|G[ABD-ILMNP-UWY]|H[KMNRTU]|I[DEMNOQRT]|J[EMO]|K[EGHIMNPWYZ]|L[ABCIKR-VY]|M[AC-HK-Z]|N[ACEFGILPRU]|OM|P[AE-HK-NRSTWY]|QA|R[EOSUW]|S[A-EG-ORSTVXYZ]|T[CDFGHJ-OTVWZ]|U[AGMSYZ]|V[ACEGINU]|W[FS]|Y[ET]|Z[AMW]))))";
	String PROVINCE =
			"(Ala(?:bama|ska)|Alberta|Arizona|Arkansas|British Columbia|California|Colorado|Connecticut|Delaware|District of Columbia|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Manitoba|Maryland|Massachusetts|Michigan|Minnesota|Miss(?:issippi|ouri)|Montana|Nebraska|Nevada|New(?: Brunswick| Hampshire| Jersey| Mexico| York|foundland)|Northwest Territory|Nova Scotia|Ontario|Prince Edward Island|Quebec|Saskatchewan|Yukon Territory|A[BKLRZ]|BC|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ABDEINOST]|N[BEFHJMSTVY]|ON|PE|QC|SK|YT)";
	String URL =
			"\\A(?<url>(?:(?:classpath|dns|file|filesystem|ftp|git|http|https|jabber|jdbc|jms|ldap|pkcs11|proxy|resource|rmi|sip|skype|smtp|snmp|soap|ssh|svn|udp|ws|wss|xmpp?)://|(?:ftp|www)\\.)[\\d.a-z-]+\\.[a-z]{2,6}+(?::\\d{1,5}+)?(?:/[]\\d!\"#$%&'()*+,.:;<=>?@\\[\\\\_`a-z{|}~^-]+){0,9}(?:/[]\\d!\"#$%&'()*+,.:;<=>?@\\[\\\\_`a-z{|}~^-]*)?(?:\\?[]\\d!\"#$%&'()*+,./:;<=>?@\\[\\\\_`a-z{|}~^-]*)?)";
	String URI =
			"^([_a-zA-Z][_a-zA-Z0-9]*)(:([_a-zA-Z][_a-zA-Z0-9]*))*(://)([_a-zA-Z][_a-zA-Z0-9]*)([./]([_a-zA-Z][_a-zA-Z0-9]*))";
	String E_MAIL =
			"\\A(?<url>(?:mailto:)?+[\\d!#$%&'*+./=?_`a-z{|}~^-]++@[\\d.a-z-]+\\.[a-z]{2,6}+)";
	String VAT =
			"\\G(?<url>\\b(?:(?:GB|LT|SE)(?>[ .]*\\d){12}|(?:HR|IT|LV)(?>[ .]*\\d){11}|(?:BE|BG|CZ|PL|SK)(?>[ .]*\\d){10}|(?:BG|CZ|DE|EE|EL|GR|GB|LT|PT)(?>[ .]*\\d){9}|(?:CZ|DK|FI|HU|LU|MT|SI)(?>[ .]*\\d){8}|NL(?>[ .]*[\\dA-Z]){12}|FR(?>[ .]*[\\dA-Z]){11}|(?:AT|CY|ES)(?>[ .]*[\\dA-Z]){9}|IE(?>[ .]*[\\dA-Z]){8,9}|GB(?>[ .]*[\\dA-Z]){5}|RO(?>[ .]*\\d){2,10})\\b)";
	String CURRENCY =
			"\\G(?<url>AED|AFN|ALL|AMD|ANG|AOA|ARS|AUD|AWG|AZN|BAM|BBD|BDT|BGN|BHD|BIF|BMD|BND|BOB|BRL|BSD|BTN|BWP|BYR|BZD|CAD|CDF|CHF|CLP|CNY|COP|COU|CRC|CUC|CUP|CVE|CZK|DJF|DKK|DOP|DZD|EGP|ERN|ETB|EUR|FJD|FKP|GBP|GEL|GHS|GIP|GMD|GNF|GTQ|GYD|HKD|HNL|HRK|HTG|HUF|USD)";
	String CREDIT_CARD =
			"^((VISA 4[0-9]{12}(?:[0-9]{3})?)|(MasterCard 5[1-5][0-9]{14})|(American Express 3[47][0-9]{13})|(Diners Club 3(0[0-5]|[68][0-9])[0-9]{11})|(Discover 6(?:011|5[0-9]{2})[0-9]{12})|(JCB (?:2131|1800|35\\d{3})\\d{11}))$";
	String NATIONAL_ID =
			"\\G(?<url>\\b[012345678]\\d{2}-\\d{2}-\\d{4}\\b)";
	String IPv4 =
			"\\G(?<url>[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})";
	String GUID =
			 "\\G(?<url>\\{[\\dA-F]{8}-[\\dA-F]{4}-[\\dA-F]{4}-[\\dA-F]{4}-[\\dA-F]{12}\\})";
}

enum RegExType {
	PHONE_NUMBER(String.class.getName() + "(Phone Number)", 	RegExPattern.PHONE_NUMBER),
	COUNTRY(String.class.getName() + "(Country)", 				RegExPattern.COUNTRY),
	PROVINCE(String.class.getName() + "(State or Province)",	RegExPattern.PROVINCE),
	URL(java.net.URL.class.getName(), 							RegExPattern.URL),	
	URI(java.net.URI.class.getName(), 							RegExPattern.URI),
	E_MAIL(java.net.URI.class.getName() + "(e-mail)", 			RegExPattern.E_MAIL),
	VAT(String.class.getName() + "(VAT)", 						RegExPattern.VAT),
	CURRENCY(String.class.getName() + "(Currency)", 			RegExPattern.CURRENCY),
	CREDIT_CARD(String.class.getName() + "(Credit Card Number)",RegExPattern.CREDIT_CARD),
	NATIONAL_ID(String.class.getName() + "(National ID)", 		RegExPattern.NATIONAL_ID),
	IPv4(java.net.Inet4Address.class.getName(), 				RegExPattern.IPv4),
	GUID(java.util.UUID.class.getName() + "(GUID)", 			RegExPattern.GUID);
	
	protected final String className;
    
	protected final String regExPattern;
	
	/**
 	 * get name of class type
 	 * @return full class name of the class
 	 */
    String getName() {
    	return className;
     }
    
    RegExType(final String className, final String regExPattern) { 
    	this.className = className;
       	this.regExPattern = regExPattern;
    }
    
	String parse(String value) {
		return parse(value, regExPattern);
	}
	
	/**
 	 * define regular string type by it string representation
 	 * @param value
 	 * @return String value in upperCase if OK or NULL if not validate 
 	 * @throw IllegalArgumentException, PatternSyntaxException
 	 */
 	private String parse(final String value, String regExPattern) { 
 		if(value == null) return null;
 		if(value.matches(regExPattern)) {
 			return value; 
 		} else {
 			throw new IllegalArgumentException();
 		}
	}
 	/* this function design for fulfillment of class API and don't use just now */
 	static boolean validate(String value, String regExPattern) { 
 		return Arrays.asList(RegExType.values()).stream()
 				.filter((type) -> (type.parse(value) != null))
 				.count() > 0;
 	}
 	
}
