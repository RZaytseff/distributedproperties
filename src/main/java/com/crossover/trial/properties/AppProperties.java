package com.crossover.trial.properties;

import java.util.List;

/**
 * Generic interface for access loaded system properties.
 *
 * Note: Candidates should not change this interface.
 *
 * @author code test administrator
 * 
 * Some methods are added by me to test fulfillment aims
 * 
 * @update by Roman.Zaytseff
*/
public interface AppProperties {

    /**
     * @return a list of properties that are unset either because they are missing or because they have the wrong type
     */
    List<String> getMissingProperties();

    /**
     * @return a list of all known keys
     */
    List<?> getKnownProperties();

    /**
     * @return true if the configuration is valid, false otherwise
     */
    boolean isValid();

    /**
     * @return set the correctness of configuration
     */
	void setValid(boolean valid);

    /**
     * resets all loaded properties to null / unloaded
     */
    void clear();

    /**
     *  Retrieve the property for the given key. Keys are case-insenstive
     *  and the use of . and _ in property names is interchangable. For example,
     *  jpa.showSQL, jpa_showsql and JPA_showSql should all retrieve the same value.
     *
     * @param key a property key, handled without case sensitivity. '.' and '_' are
     *            treated as equivalent
     * @return an object of the given key or null if it is not available
     */
    Object get(String key);

    /**
     *  set the property value for the given key. Keys are case-insenstive
     *  and the use of . and _ in property names is interchangable. For example,
     *  jpa.showSQL, jpa_showsql and JPA_showSql should all retrieve the same value.
     *
     * @param key a property key, handled without case sensitivity. '.' and '_' are
     *            treated as equivalent
     */
	void putProperty(String k, String v);

	String getProperty(String k);

	/**
	 * method is assign to get original script of key
	 * @param key - any script of key with any combination of literals, digits and symbols "_" and "."
	 * @return keyScript - the original script of key such as written in properties resource 
	 */
	String getKeyScript(String key);

	/**
	 * set the property key. Keys are case-sensitive and '.' and '_' are
     *            treated as not equivalent in
	 * 
	 * @param missing - key list of missing properties are separated by "," symbol
	 */
	void putMissingProperties(String missing);

}
