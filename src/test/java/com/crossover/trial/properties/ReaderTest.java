package com.crossover.trial.properties;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.stream.Stream;

import org.junit.Test;

import com.crossover.trial.properties.reader.ResourceReaderFabrica;
import com.google.gson.JsonSyntaxException;

/**
 * Testing:
 * 1. http read operation for correct and not correctness data
 * 2. check replacement feature if property from some resources have equal key
 * 
 * @author Roman.Vit.Zaitseff
 *
 */
public class ReaderTest {
	// some urls are temporary So they must be refreshed some time later
	private final String urlJson        = "http://jsonplaceholder.typicode.com/posts/1?test.json";
	private final String urlJsonList    = "http://jsonplaceholder.typicode.com/users?userList.json";
	private final String urlProperties  = "https://download.jitsi.org/jitsi/provisioning-mac.properties";
	private final String urlXML         = "https://download.jitsi.org/jitsi/macosx/sparkle/updates.xml";
	private final String urlXMLProps    = "https://download.jitsi.org/jitsi/macosx/sparkle/updates.xml?test.properties";
	
	private final String fileJson       = "file:resources/aws.json";
	private final String fileProperties = "file:resources/jdbc.properties";
	private final String fileList       = "file:resources/desired.properties.list";
	private final String fileKeyList    = "file:resources/desiredList.properties";
	
	private final String classpathProps = "classPath:jdbc.properties";
	private final String classpathJson  = "classPath:aws.json";
	private final String classpathList  = "classpath:desired.properties.list";
	private final String classpathKeyList = "classpath:desiredList.properties";
	
	private final String defaultName    = "resources/aws.json";

	private final String systemURI      = "system:global.properties";
	
	/**
	 * web-service "http://jsonplaceholder.typicode.com/posts/1?test.json"
	 * must return from this address the following values:
	{
	{"userId", "1"}, {"id", "1"},
	{"title", "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"},
	{"body", "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"}
	};
	 */		
	@Test
	public void jsonPropertiesFromHttpTest() throws UnsupportedOperationException, FileNotFoundException, IOException, URISyntaxException, JsonSyntaxException  {
		Properties res = ResourceReaderFabrica.gather(urlJson);
		assertTrue("1".equals(res.get("id")));
		assertTrue("1".equals(res.get("userId")));
	}

	/** try to read JSON Array object from HTTP resource */
	@Test
	public void jsonArrayFromHttpTest() {
		try {
			ResourceReaderFabrica.gather(urlJsonList);
			assertFalse(false);
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	/** try to read properties from HTTP resource 
	 * 
	 * @throws IOException 
	 * @throws JsonSyntaxException */
	@Test
	public void propertyFromHttpTest() throws JsonSyntaxException, IOException {
			Properties res = ResourceReaderFabrica.gather(urlProperties); 
			assertTrue(res.getProperty("net.java.sip.communicator.UPDATE_LINK") != null);
	}

	/** 
	 * try to read properties from http url 
	 * with correct and not correct uri suffix
	 * 
	 * @throws IOException 
	 * @throws JsonSyntaxException */
	@Test
	public void xmlFromHttpTest() throws JsonSyntaxException, IOException {
			Properties res = ResourceReaderFabrica.gather(urlXML); 
			assertTrue(res.size() == 0);
			
			res = ResourceReaderFabrica.gather(urlXMLProps); 
			assertTrue(res.size() > 0);
	}

	/**
	 * read correct data from file in PROPERTIES format
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	@Test
	public void filePropertiesReaderTest() throws JsonSyntaxException, IOException {
		Properties res = ResourceReaderFabrica.gather(fileProperties); 
		assertTrue(res.size() > 0);
		assertTrue(res.getProperty("JDBC_URL") != null);
	}

	/**
	 * read correct data from file in JSON format
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	@Test
	public void fileJsonReaderTest() throws JsonSyntaxException, IOException {
		Properties res = ResourceReaderFabrica.gather(fileJson); 
		assertTrue(res.size() > 0);
		assertTrue(res.getProperty("aws.region_id") != null);
	}

	/**
	 * try to read  from file uri with incorrect suffix
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@Test
	public void fileListReaderTest() throws JsonSyntaxException, IOException {
		Properties res = ResourceReaderFabrica.gather(fileList); 
		assertTrue(res.size() == 0);
	}
	
	/**
	 * try to read not properly format date with correct suffix file URI
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@Test
	public void fileKeyListReaderTest() throws JsonSyntaxException, IOException, URISyntaxException {
		Properties res = ResourceReaderFabrica.gather(fileKeyList); 
		int lineCount = (int) new BufferedReader(new InputStreamReader(new URL(fileKeyList).openStream())).lines().count();
		assertTrue(res.size() == lineCount); 
		// reopen stream
		Stream<String> lines = new BufferedReader(new InputStreamReader(new URL(fileKeyList).openStream())).lines();
		lines.forEach((line) -> assertTrue(res.getProperty(line.split(" ")[0]) != null));
	}

	/**
	 * read correct data from classpath in PROPERTIES format
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	@Test
	public void classpathPropertiesReaderTest() throws JsonSyntaxException, IOException {
		Properties res = ResourceReaderFabrica.gather(classpathProps); 
		assertTrue(res.size() > 0);
		assertTrue(res.getProperty("JDBC_URL") != null);
	}

	/**
	 * read correct data from file in JSON format
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	@Test
	public void classpathJsonReaderTest() throws JsonSyntaxException, IOException {
		Properties res = ResourceReaderFabrica.gather(classpathJson); 
		assertTrue(res.size() > 0);
		assertTrue(res.getProperty("aws.region_id") != null);
	}

	/**
	 * try to read  from file uri with incorrect suffix
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@Test
	public void classpathListReaderTest() throws JsonSyntaxException, IOException {
		Properties res = ResourceReaderFabrica.gather(classpathList); 
		assertTrue(res.size() == 0);
	}
	
	/**
	 * try to read not properly format date with correct suffix file URI
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@Test
	public void classpathKeyListReaderTest() throws JsonSyntaxException, IOException, URISyntaxException {
		Properties res = ResourceReaderFabrica.gather(classpathKeyList); 
		String source = classpathKeyList.substring(10);
		int lineCount = (int) new BufferedReader(new InputStreamReader(
				ReaderTest.class.getClassLoader().getResource(source).toURI().toURL().openStream())).lines().count();
		assertTrue(res.size() == lineCount); 
		// reopen stream
		Stream<String> lines = new BufferedReader(new InputStreamReader(
				ReaderTest.class.getClassLoader().getResource(source).toURI().toURL().openStream())).lines();
		lines.forEach((line) -> assertTrue(res.getProperty(line.split(" ")[0]) != null));
	}
	
	/**
	 * read default URI
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	@Test
	public void defaultJsonReaderTest() throws JsonSyntaxException, IOException {
		Properties res = ResourceReaderFabrica.gather(defaultName); 
		assertTrue(res.size() > 0);
		assertTrue(res.getProperty("aws.region_id") != null);
	}

	/**
	 * read system properties URI
	 * Of course, if this program is executed,
	 * then java runtime environments is set 
	 * 
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	@Test
	public void systemPropertiesReaderTest() throws JsonSyntaxException, IOException {
		Properties res = ResourceReaderFabrica.gather(systemURI); 
		assertTrue(res.size() > 0);
		assertTrue(res.getProperty("java.runtime.name") != null);
	}


}
