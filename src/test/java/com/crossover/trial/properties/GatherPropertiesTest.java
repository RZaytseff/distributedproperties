package com.crossover.trial.properties;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Properties;

import org.junit.Test;

import com.crossover.trial.properties.reader.ResourceReaderFabrica;
import com.crossover.trial.properties.type.PropertyType;

/**
 * Test class use properties that lie in 'resources' folder 
 * and permanent Internet address to obtain correctness of test's data
 * 
 * @author Roman.Zaytseff
 */
public class GatherPropertiesTest {
	
	private final String uris 
			= "classpath:jdbc.properties;"
			+ "file:resources/aws.json;"
    		+ "file:resources/jdbc.properties;"
    		+ "http://jsonplaceholder.typicode.com/posts/1?test.json;"
    		+ "classpath:config.json;"
			+ "system:environment.properties;"
   		;

	
	/**
	 * Read property from  file with correct property description
	 * and check correctness of his value and definition of his type 
	 */
	@Test
	public void readResolvedPropertiesTest() {
        AppPropertiesManager m = new TrialAppPropertiesManager();
        AppProperties props = m.loadProps(Arrays.asList(uris.split(";")));
        m.printProperties(props, System.out);
        
        assertTrue(props.get("jpa.showSql") instanceof Boolean);
        assertTrue("false".equalsIgnoreCase("" + props.get("jpa.showSql")));
        assertTrue(Boolean.class.getName().equals
        		(PropertyType.type("jpa_showSql", "" + props.get("JPA.ShowSql")))); 
        
        assertTrue(TypeSafeAccessor.valueOf("jpa.showSql") instanceof Boolean);
        assertTrue(!TypeSafeAccessor.booleanValue("jpa.showSql"));

	}
	
	/**
	 * Read property from  file with incorrect or empty property description
	 * For example,
	 * 
	 * Correctness: 
	 * ----------------------------
	 *  JPA_Hibernate_SQL_SHOW=true
	 *  	or
	 *  "JPA_Hibernate_SQL_SHOW"="false"
	 *  	or
	 *  JPA_Hibernate_SQL_SHOW=""
	 *  
	 *  Incorrectness:
	 *  ---------------------------
	 *  JPA_Hibernate_SQL_SHOW=(null)
	 *  	or
	 *  JPA_Hibernate_SQL_SHOW(null)
	 *  	or
	 *  (null)='false'
	 */
	@Test
	public void readUnresolvedPropertiesTest() {
        AppPropertiesManager m = new TrialAppPropertiesManager();
        AppProperties props = m.loadProps(Arrays.asList(uris.split(";")));
        assertTrue(props.getMissingProperties().contains("JPA_Hibernate_SQL_SHOW"));

	}
	
	/**
	 * Check property value after replace it 
	 * from  second file with the same property key.
	 * 
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws UnsupportedOperationException 
	 */
	@Test
	public void replacePropertyTest() throws UnsupportedOperationException, FileNotFoundException, IOException, URISyntaxException {
		String pathFirst  = "file:resources/jdbc.properties";
		String pathSecond = "file:resources/config.json";
		Properties p = ResourceReaderFabrica.gather(pathFirst);
		assertTrue("true".equalsIgnoreCase((String)p.get("JPA_SHOWSQL")));
		p = ResourceReaderFabrica.gather(pathSecond);
		assertTrue("false".equalsIgnoreCase((String)p.get("jpa.showSql")));
	}

}
