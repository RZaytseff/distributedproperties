package com.crossover.trial.properties;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

/**
 * This class allows run tests from command line
 * 
 * @author Roman.Zaytseff
 */
public class AllTests {
	
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		Class[] testClasses = {
				TypeTest.class,
				ReaderTest.class,
				GatherPropertiesTest.class,
				TypeSafeAccessorTest.class
				};
		
		Result resultTest;
		try {
			Class.forName(JUnitCore.class.getName());
			(resultTest = JUnitCore.runClasses(testClasses)).getFailures()
					.forEach((fail) -> System.out.println(fail.toString()));

			if (resultTest != null && resultTest.wasSuccessful()) {
				System.out.println("All tests finished successfully");
			} else {
				System.out.println("-------------------> TESTS FAIL");
			}
				
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
