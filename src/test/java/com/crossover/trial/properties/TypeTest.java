package com.crossover.trial.properties;

import static org.junit.Assert.*;

import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.*;

import org.junit.*;

import com.crossover.trial.properties.type.PropertyType;

/** 
 * testing correctness of recognizing of various data types and data format
 * 
 * @author Roman.Zaytseff
 */
public class TypeTest {
	
	@Test
	public void localDateTimeTest() {
		String date = "2014-04-28";
		assertTrue(LocalDate.class.getName().equals(PropertyType.valueType(date)));
		assertTrue(PropertyType.value(date) instanceof LocalDate);	
		assertTrue(date.equals("" + PropertyType.value(date)));	

		String time = "12:24:36";
		assertTrue(LocalTime.class.getName().equals(PropertyType.valueType(time)));
		assertTrue(PropertyType.value(time) instanceof LocalTime);	
		assertTrue(time.equals("" + PropertyType.value(time)));	

		String dateTime = "2016-03-01T04:17:24.330";  
		assertTrue(LocalDateTime.class.getName().equals(PropertyType.valueType(dateTime))); 
		assertTrue(PropertyType.value(dateTime) instanceof LocalDateTime);	
		assertTrue(dateTime.equals("" + PropertyType.value(dateTime)));	

		String timeStamp = "2016-03-01T01:50:45.710Z"; 
		assertTrue(Instant.class.getName().equals(PropertyType.valueType(timeStamp))); 
		assertTrue(PropertyType.value(timeStamp) instanceof Instant);	
		assertTrue(timeStamp.equals("" + PropertyType.value(timeStamp)));	

		String zonedDateTime = "2016-03-01T04:22:16.787+03:00[Europe/Moscow]";  
		assertTrue(ZonedDateTime.class.getName().equals(PropertyType.valueType(zonedDateTime))); 
		assertTrue(PropertyType.value(zonedDateTime) instanceof ZonedDateTime);	
		assertTrue(zonedDateTime.equals("" + PropertyType.value(zonedDateTime)));	
	}
	
	@Test
	public void dateTest() {
		final String dateType = Date.class.getName();
		final String[] examples = {
			"Thu Sep 28 20:29:30 PST 2000",
			"27-09-1991 20:29:30",
			"7-Jun-2013",
			"07/06/2013",
			"Jun 7, 2013",
			"Fri, June 7 2013",
			"Friday, Jun 7, 2013 12:10:56 PM",
			"04 02, 2014"
		};
		
		Arrays.asList(examples).forEach((formatExample) 
			-> {assertTrue(dateType.equals(PropertyType.type("update", formatExample)));
				assertTrue(PropertyType.value(formatExample) instanceof Date);					
				}
			);
		
	}
	
	@Test
	public void baseTypesTest() {
		final String[][] typeExamples = {
				{Boolean.class.getName(), "true"},
				{Boolean.class.getName(), "false"},
				{Integer.class.getName(), "12345678"},
				{Integer.class.getName(),"-12345678"},
				{Long.class.getName(),    "1234567890123456789"},
				{Long.class.getName(),   "-1234567890123456789"},
				{Float.class.getName(),   "1.2345678"},
				{Float.class.getName(),  "-1.2345678"},
				{String.class.getName(),"\"1.2345678\""},
				{String.class.getName(), "\"-123456789012345678.90 - this is string\""},
				{String.class.getName(),   "1.2345678.90123456789"}
		};
		
		Arrays.asList(typeExamples).forEach((typeExample) 
				-> {assertTrue(typeExample[0].equals(PropertyType.valueType(typeExample[1])));
					assertTrue(typeExample[1].equalsIgnoreCase("" + PropertyType.value(typeExample[1])));
					assertTrue(typeExample[0].equalsIgnoreCase(PropertyType.value(typeExample[1]).getClass().getName()));					
					}
				);
	}
	
	@Test
	public void currencyTest() {
		String[] currencies = {"USD", "CAD", "EUR", "GBP"};
		Arrays.asList(currencies).forEach((currency) 
				-> {assertTrue(PropertyType.CURRENCY.getType().equals(PropertyType.valueType(currency)));
					assertTrue(currency.equalsIgnoreCase("" + PropertyType.value(currency)));
					assertTrue(PropertyType.CURRENCY.getType().contains(PropertyType.value(currency).getClass().getName()));					
					}
				);
	}
	
	@Test
	public void countryTest() {
		String[] countryCodes = {"US", "FR", "GB", "GB", "GE"};
		String[] countryNames = {"France", "CANADA", "Great Britain", "Germany", "United States", "England"};

		Arrays.asList(countryCodes).forEach((country) 
				-> {assertTrue(PropertyType.COUNTRY.getType().equals(PropertyType.valueType(country)));
					assertTrue(country.equalsIgnoreCase("" + PropertyType.value(country)));
					assertTrue(PropertyType.COUNTRY.getType().contains(PropertyType.value(country).getClass().getName()));					
					}
				);
		
		
		Arrays.asList(countryNames).forEach
			((country) -> assertTrue(PropertyType.COUNTRY.getType().equalsIgnoreCase(PropertyType.valueType(country))
							|| PropertyType.COUNTRY_NAME.getType().equalsIgnoreCase(PropertyType.valueType(country)))
			);
	}
	
	@Test
	public void stateProvinceTest() {
		String[] provinces = {"British Columbia", "California", "Florida"};

		Arrays.asList(provinces).forEach((province) 
				-> {assertTrue(PropertyType.STATE.getType().equals(PropertyType.valueType(province)));
					assertTrue(province.equalsIgnoreCase("" + PropertyType.value(province)));
					assertTrue(PropertyType.STATE.getType().contains(PropertyType.value(province).getClass().getName()));					
					}
				);
	}

	@Test
	public void urlTest() {
		String[] urls = {"http://www.regexmagic.com"
							, "http://regexmagic.com"
							, "http://www.regexmagic.com"
							, "http://www.regexmagic.com/"
							, "http://www.regexmagic.com/index.html"
							, "http://www.regexmagic.com/index.html?source=library"
		};
		
		Arrays.asList(urls).forEach( 
				(url) -> {assertTrue(PropertyType.URL.getType().equals(PropertyType.valueType(url)));
							assertTrue(url.equalsIgnoreCase("" + PropertyType.value(url)));
							assertTrue(PropertyType.value(url) instanceof URL);
							}
			);
	}
	
	@Test
	public void eMailTest() {
		String[] urls = { "support@regexmagic.com"
							, "support@regexmagic.hotmail.com.ua"
							,  "office.admin.support@regexmagic.hotmail.com.ua"
		};
		
		Arrays.asList(urls).forEach( 
				(url) -> assertTrue(PropertyType.E_MAIL.getType().equalsIgnoreCase(PropertyType.valueType(url)))
			);
		
		String e_mail_url = "mailto:office.admin.support@regexmagic.hotmail.com.ua";
		assertTrue(URL.class.getName().equals(PropertyType.valueType(e_mail_url)));
	}
	
	@Test
	public void vatTest() {
		String[] vats = { "ATU12345678"
							, "CZ1234567890"
							, "FI12345678"
							, "SK1234567890"
		};
		
		Arrays.asList(vats).forEach( 
				(vat) -> assertTrue(PropertyType.VAT.getType().equals(PropertyType.valueType(vat)))
			);
	}
	
	@Test
	public void creditCardTest() {
		String[] cards = { "VISA 4123456789012" 
							, "VISA 4123456789012345"
							, "MasterCard 5512345678901234" 
							, "American Express 341234567890123" 
							, "Diners Club 30012345678901" 
		};
		
		Arrays.asList(cards).forEach( 
				(card) -> assertTrue(PropertyType.CREDIT_CARD.getType().equals(PropertyType.valueType(card)))
			);
	}
	
	public void nationalIdTest() {
		String[] valid_IDs = { "123-12-1234"
								, "078-05-1120"
 
		};
		
		Arrays.asList(valid_IDs).forEach( 
				(id) -> assertTrue(PropertyType.NATIONAL_ID.getType().equals(PropertyType.valueType(id)))
			);
		
		String[] invalid_IDs = { "123121234"
								, "1234-12-1234"
								, "000-00-0000"
		};

		Arrays.asList(invalid_IDs).forEach( 
				(id) -> assertFalse(PropertyType.NATIONAL_ID.getType().equals(PropertyType.valueType(id)))
			);
	}
	
	public void ipV4Test() {
		String[] ips = { "10.1.1.1"
								, "10.255.255.254"
								, "172.0.0.1"
								, "172.31.255.254"
								, "192.168.1.1"
								, "192.168.255.254"
								, "8.8.8.8"
		};
		
		Arrays.asList(ips).forEach( 
				(ip) -> assertTrue(PropertyType.NATIONAL_ID.getType().equals(PropertyType.valueType(ip)))
			);
		
	}
	
	public void guidTest() {
		String[] guids = { "{D1A5279D-B27D-4CD4-A05E-EFDD53D08E8D}"
							, "{99367d71-08ad-4aec-8e73-55ae151614f9}"
							, "b4b2fb69c6244e5eb0698e0c6ec66618"
							, "0c74f13f-fa83-4c48-9b33-68921dd72463"
							, "DE011585-7317-4C0D-A9AF-0AF2C5E61DB8}"
							, "{E8038A99-A409-490B-B11A-159A3496426D"
							, "2F2B15A5-2615-4748-BDABA124210F15EC"
		};
		
		Arrays.asList(guids).forEach( 
				(guid) -> {assertTrue(PropertyType.GUID.getType().equals(PropertyType.valueType(guid)));
							assertTrue(guid.equals("" + PropertyType.value(guid)));
							assertTrue(PropertyType.value(guid) instanceof java.util.UUID);
							}
			);
		
	}
	
	@Test
	public void phoneNumberTest() {
		String phone = "+1(510)555-1212";
		assertTrue(PropertyType.PHONE_NUMBER.getType().equals(PropertyType.valueType(phone)));
		assertTrue(phone.equals(PropertyType.value(phone)));
		
		phone = "1(510)555-1212";
		assertFalse(PropertyType.PHONE_NUMBER.getType().equals(PropertyType.valueType(phone)));
	}
	
	@Test
	public void regionTest() {
		String[][] AWS = {
			{"aws_region_id", "us-west-1"},
			{"aws.Region_id", "us-west-2"},
			{"aws.region",    "us-gov-west-1"}
		};
		
		Arrays.asList(AWS).forEach((kv) 
			-> assertTrue(PropertyType.AWS_REGION.toString().equals(PropertyType.type(kv[0], kv[1]))));

		String[][] notAWS = {
				{"aws_region_id", "us-west-3"},
				{"awsRegion",     "us-east-2"},
				{"aws.region",    "us-east-1"}
			};
		Arrays.asList(notAWS).forEach((kv) 
				-> assertTrue(!PropertyType.AWS_REGION.toString().equals(PropertyType.type(kv[0], kv[1]))));

	}
	
//	Regions:	
//	[GovCloud, US_EAST_1, US_WEST_1, US_WEST_2, EU_WEST_1, EU_CENTRAL_1, AP_SOUTHEAST_1, AP_SOUTHEAST_2, AP_NORTHEAST_1, AP_NORTHEAST_2, SA_EAST_1, CN_NORTH_1]
	
			@Test
			public void regionsTest() {
				String[][] AWS = {
					{"aws_region_id", "GovCloud"},
					{"aws.Region_id", "US_EAST_1"},
					{"aws.region",    "CN_NORTH_1"}
				};
				
				Arrays.asList(AWS).forEach((kv) 
					-> assertTrue(PropertyType.AWS_REGIONS.toString().equals(PropertyType.type(kv[0], kv[1]))));

				String[][] notAWS = {
						{"aws_region_id", "us-gov-west-1"},
						{"aws.Region_id", "us-east-1"},
						{"aws.region",    "us-west-1"}
					};
				Arrays.asList(notAWS).forEach((kv) 
						-> assertTrue(!PropertyType.AWS_REGIONS.toString().equals(PropertyType.type(kv[0], kv[1]))));

			}
			
}
