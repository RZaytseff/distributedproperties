package com.crossover.trial.properties;

import static org.junit.Assert.*;

import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.*;

import org.junit.*;

import com.amazonaws.services.s3.model.Region;
import com.crossover.trial.properties.type.PropertyType;

/** 
 * testing correctness of recognizing of various data types and data format
 * 
 * @author Roman.Vit.Zaitseff
 */
public class TypeSafeAccessorTest {
	
	TrialAppProperties trialAppProperties = new TrialAppProperties();
	
	@Test
	public void localDateTimeTest() {
		String date = "2014-04-28";
		trialAppProperties.putProperty("date", date);
		assertTrue(trialAppProperties.get("date") instanceof LocalDate);
		assertTrue(TypeSafeAccessor.valueOf("date") instanceof LocalDate);
		assertTrue(date.equals("" + TypeSafeAccessor.localDateValue("date")));
		assertTrue(date.equals("" + trialAppProperties.get("date")));
		
		// test not correct date
		String errorDate = "2014-14-28";
		trialAppProperties.putProperty("date", errorDate);
		assertFalse(TypeSafeAccessor.valueOf("date") instanceof LocalDate);
		assertTrue(TypeSafeAccessor.valueOf("date") instanceof String);
		assertFalse(trialAppProperties.get("date") instanceof LocalDate);
		assertTrue(trialAppProperties.get("date")  instanceof String);	
		try {
			assertTrue(errorDate.equals("" + TypeSafeAccessor.localDateValue("date")));
			assertFalse(true);
		} catch (IllegalArgumentException e) {
			assertFalse(false);
		}
		assertTrue(errorDate.equals("" + trialAppProperties.get("date")));	

		String time = "12:24:36";
		trialAppProperties.putProperty("time", time);
		assertTrue(trialAppProperties.get("time") instanceof LocalTime);
		assertTrue(time.equals("" + TypeSafeAccessor.localTimeValue("time")));
		assertTrue(time.equals("" + trialAppProperties.get("time")));	

		// test not correct time
		String errorTime = "32:24:36";
		trialAppProperties.putProperty("time", errorTime);
		assertFalse(trialAppProperties.get("time") instanceof LocalTime);
		assertTrue(trialAppProperties.get("time")  instanceof String);	
		try {
			assertTrue(errorTime.equals("" + TypeSafeAccessor.localTimeValue("time")));
			assertFalse(true);
		} catch (IllegalArgumentException e) {
			assertFalse(false);
		}
		assertTrue(errorTime.equals("" + trialAppProperties.get("time")));	

		String dateTime = "2016-03-01T04:17:24.330";  
		trialAppProperties.putProperty("dateTime", dateTime);
		assertTrue(trialAppProperties.get("dateTime") instanceof LocalDateTime);	
		assertTrue(dateTime.equals("" + TypeSafeAccessor.localDateTimeValue("dateTime")));
		assertTrue(dateTime.equals("" + trialAppProperties.get("dateTime")));	

		// test not correct dateTime
		String errorDateTime = "2016-02-30T04:17:24.330";  
		trialAppProperties.putProperty("dateTime", errorDateTime);
		assertFalse(trialAppProperties.get("dateTime") instanceof LocalDateTime);	
		assertTrue(trialAppProperties.get("dateTime") instanceof String);
		try {
			assertTrue(dateTime.equals("" + TypeSafeAccessor.localDateTimeValue("dateTime")));
			assertFalse(true);
		} catch (IllegalArgumentException e) {
			assertFalse(false);
		}
		assertTrue(errorDateTime.equals("" + trialAppProperties.get("dateTime")));	

		String timeStamp = "2016-03-01T01:50:45.710Z"; 
		trialAppProperties.putProperty("timeStamp", timeStamp);
		assertTrue(PropertyType.value(timeStamp) instanceof Instant);	
		assertTrue(timeStamp.equals("" + TypeSafeAccessor.timeStampsValue("timeStamp")));
		assertTrue(timeStamp.equals("" + PropertyType.value(timeStamp)));	

		// test not correct timeStamp
		String errorTimeStamp = "2016-03-01T01:50:65.710Z"; 
		trialAppProperties.putProperty("timeStamp", errorTimeStamp);
		assertFalse(PropertyType.value(errorTimeStamp) instanceof Instant);	
		assertTrue (PropertyType.value(errorTimeStamp) instanceof String);
		try {
			assertTrue(errorTimeStamp.equals("" + TypeSafeAccessor.timeStampsValue("timeStamp")));
			assertFalse(true);
		} catch (IllegalArgumentException e) {
			assertFalse(false);
		}
		assertTrue(errorTimeStamp.equals("" + PropertyType.value(errorTimeStamp)));	

		
		String zonedDateTime = "2016-03-01T04:22:16.787+03:00[Europe/Moscow]";  
		trialAppProperties.putProperty("zonedDateTime", zonedDateTime);
		assertTrue(trialAppProperties.get("zonedDateTime") instanceof ZonedDateTime);	
		assertTrue(zonedDateTime.equals("" + TypeSafeAccessor.zonedDateTimeValue("zonedDateTime")));
		assertTrue(zonedDateTime.equals("" + trialAppProperties.get("zonedDateTime")));	

		// test not correct zonedDateTime
		String errorZonedDateTime = "2016-02-30T04:22:16.787+03:00[Europe/Moscow]";  
		trialAppProperties.putProperty("zonedDateTime", errorZonedDateTime);
		assertFalse(trialAppProperties.get("zonedDateTime") instanceof ZonedDateTime);	
		assertTrue (trialAppProperties.get("zonedDateTime") instanceof String);
		try {
			assertTrue(errorZonedDateTime.equals("" + TypeSafeAccessor.zonedDateTimeValue("zonedDateTime")));
			assertFalse(true);
		} catch (IllegalArgumentException e) {
			assertFalse(false);
		}
		assertTrue(errorZonedDateTime.equals("" + trialAppProperties.get("zonedDateTime")));	
}
	
	@Test
	public void dateTest() {
		final String[] dateExamples = {
			"Thu Sep 28 20:29:30 PST 2000",
			"27-09-1991 20:29:30",
			"7-Jun-2013",
			"07/06/2013",
			"Jun 7, 2013",
			"Fri, June 7 2013",
			"Friday, Jun 7, 2013 12:10:56 PM",
			"04 02, 2014"
		};
		
		Arrays.asList(dateExamples).forEach((date) 
			-> {trialAppProperties.putProperty("date", date);
				assertTrue(trialAppProperties.get("date") instanceof Date);					
				}
			);
		
	}
	
	@Test
	public void notCorrectDateTest() {
		final String date = "Thu Sep 28 20:59:30 QST 2000";
		trialAppProperties.putProperty("date", date);
		assertFalse(trialAppProperties.get("date") instanceof Date);
		assertTrue (trialAppProperties.get("date") instanceof String);
	}
	
	@Test
	public void baseTypesTest() {
		final String[][] typeExamples = {
				{Boolean.class.getName(), "true"},
				{Boolean.class.getName(), "false"},
				{Integer.class.getName(), "12345678"},
				{Integer.class.getName(),"-12345678"},
				{Long.class.getName(),    "1234567890123456789"},
				{Long.class.getName(),   "-1234567890123456789"},
				{Float.class.getName(),   "1.2345678"},
				{Float.class.getName(),  "-1.2345678"},
				{String.class.getName(),"\"1.2345678\""},
				{String.class.getName(), "\"-123456789012345678.90 - this is a string\""},
				{String.class.getName(),   "1.2345678.90123456789"}
		};
		
		Arrays.asList(typeExamples).forEach((typeExample) 
				-> {trialAppProperties.putProperty("baseType", typeExample[1]);
				
					assertTrue(typeExample[1].equals("" + TypeSafeAccessor.valueOf("baseType")));
					
					assertTrue(typeExample[1].equalsIgnoreCase("" + trialAppProperties.get("baseType")));
					
					assertTrue(typeExample[0].equalsIgnoreCase(trialAppProperties.get("baseType").getClass().getName()));					
					}
				);
	}
	
	@Test
	public void currencyTest() {
		String[] currencies = {"USD", "CAD", "EUR", "GBP"};
		Arrays.asList(currencies).forEach((currency) 
				-> {trialAppProperties.putProperty("currency", currency);
					assertTrue(currency.equalsIgnoreCase("" + trialAppProperties.get("currency")));
					assertTrue(PropertyType.CURRENCY.getType().contains(trialAppProperties.get("currency").getClass().getName()));					
					}
				);
	}
	
	@Test
	public void errorCurrencyTest() {
		String currency = "USA";
		trialAppProperties.putProperty("currency", currency);
		
		assertTrue(currency.equalsIgnoreCase("" + trialAppProperties.get("currency")));
		assertTrue(PropertyType.STRING.getType().equals(trialAppProperties.get("currency").getClass().getName()));					
	}
	
	@Test
	public void countryTest() {
		String[] countryCodes = {"US", "FR", "GB", "GB", "GE"};
		String[] countryNames = {"France", "CANADA", "Great Britain", "Germany", "United States", "England"};

		Arrays.asList(countryCodes).forEach((country) 
				-> {trialAppProperties.putProperty("country", country);
					assertTrue(country.equalsIgnoreCase("" + trialAppProperties.get("country")));
					assertTrue(country.equalsIgnoreCase("" + trialAppProperties.get("country")));
					assertTrue(PropertyType.COUNTRY.getType().contains(trialAppProperties.get("country").getClass().getName()));					
					}
				);
		
		
		Arrays.asList(countryNames).forEach((country) 
					-> {trialAppProperties.putProperty("country", country);
							assertTrue(PropertyType.COUNTRY.getType().contains(trialAppProperties.get("country").getClass().getName())
							|| PropertyType.COUNTRY_NAME.getType().contains(trialAppProperties.get("country").getClass().getName()));
					}
			);
	}
	
	@Test
	public void stateProvinceTest() {
		String[] provinces = {"British Columbia", "California", "Florida"};

		Arrays.asList(provinces).forEach((province) 
				-> {trialAppProperties.putProperty("province", province);
					assertTrue(province.equalsIgnoreCase("" + trialAppProperties.get("province")));
					assertTrue(PropertyType.STATE.getType().contains(trialAppProperties.get("province").getClass().getName()));					
					}
				);
	}

	@Test
	public void urlTest() {
		String[] urls = {"http://www.regexmagic.com"
							, "http://regexmagic.com"
							, "http://www.regexmagic.com"
							, "http://www.regexmagic.com/"
							, "http://www.regexmagic.com/index.html"
							, "http://www.regexmagic.com/index.html?source=library"
		};
		
		Arrays.asList(urls).forEach( 
				(url) -> {trialAppProperties.putProperty("url", url);
							assertTrue(url.equalsIgnoreCase("" + trialAppProperties.get("url")));
							assertTrue(url.equalsIgnoreCase("" + trialAppProperties.get("url")));
							assertTrue(trialAppProperties.get("url") instanceof URL);
							}
			);
	}
	
	@Test
	public void eMailTest() {
		String[] urls = { "support@regexmagic.com"
							, "support@regexmagic.hotmail.com.ua"
							,  "office.admin.support@regexmagic.hotmail.com.ua"
		};
		
		Arrays.asList(urls).forEach( 
				(url) -> {trialAppProperties.putProperty("url", url); // URI uri = new URI("support@regexmagic.com");
							assertTrue(url.equalsIgnoreCase("" + trialAppProperties.get("url")));
							assertTrue(trialAppProperties.get("url") instanceof String);	
						}
			);
		
		String e_mail_url = "mailto:office.admin.support@regexmagic.hotmail.com.ua";
		trialAppProperties.putProperty("e-mail", e_mail_url);
		assertTrue(trialAppProperties.get("e-mail") instanceof URL);
	}
	
	@Test
	public void vatTest() {
		String[] vats = { "ATU12345678"
							, "CZ1234567890"
							, "FI12345678"
							, "SK1234567890"
		};
		
		Arrays.asList(vats).forEach( 
				(vat) -> {trialAppProperties.putProperty("vat", vat);
							assertTrue(PropertyType.VAT.getType().contains(trialAppProperties.get("vat").getClass().getName()));
				}
			);
	}
	
	@Test
	public void creditCardTest() {
		String[] cards = { "VISA 4123456789012" 
							, "VISA 4123456789012345"
							, "MasterCard 5512345678901234" 
							, "American Express 341234567890123" 
							, "Diners Club 30012345678901" 
		};
		
		Arrays.asList(cards).forEach( 
				(card) -> {trialAppProperties.putProperty("creditCard", card);
							assertTrue(PropertyType.CREDIT_CARD.getType().contains(trialAppProperties.get("creditCard").getClass().getName()));
						}
			);
	}
	
	public void nationalIdTest() {
		String[] valid_IDs = { "123-12-1234"
								, "078-05-1120"
 
		};
		
		Arrays.asList(valid_IDs).forEach( 
				(id) -> {trialAppProperties.putProperty("id", id);
							assertTrue(PropertyType.NATIONAL_ID.getType().contains(trialAppProperties.get("id").getClass().getName()));
						}
			);
		
		String[] invalid_IDs = { "123121234"
								, "1234-12-1234"
								, "000-00-0000"
		};

		Arrays.asList(invalid_IDs).forEach( 
				(id) -> {trialAppProperties.putProperty("id", id);
							assertFalse(PropertyType.NATIONAL_ID.getType().contains(trialAppProperties.get("creditCard").getClass().getName()));
						}
			);
	}
	
	public void ipV4Test() {
		String[] ips = { "10.1.1.1"
								, "10.255.255.254"
								, "172.0.0.1"
								, "172.31.255.254"
								, "192.168.1.1"
								, "192.168.255.254"
								, "8.8.8.8"
		};
		
		Arrays.asList(ips).forEach( 
				(ip) -> {trialAppProperties.putProperty("ipV4", ip);
						assertTrue(PropertyType.IPv4.getType().contains(trialAppProperties.get("ip").getClass().getName()));
				}
			);
		
	}
	
	public void guidTest() {
		String[] guids = { "{D1A5279D-B27D-4CD4-A05E-EFDD53D08E8D}"
							, "{99367d71-08ad-4aec-8e73-55ae151614f9}"
							, "b4b2fb69c6244e5eb0698e0c6ec66618"
							, "0c74f13f-fa83-4c48-9b33-68921dd72463"
							, "DE011585-7317-4C0D-A9AF-0AF2C5E61DB8}"
							, "{E8038A99-A409-490B-B11A-159A3496426D"
							, "2F2B15A5-2615-4748-BDABA124210F15EC"
		};
		
		Arrays.asList(guids).forEach( 
				(guid) -> {trialAppProperties.putProperty("guid", guid);
							assertTrue(guid.equals("" + trialAppProperties.get("guid")));
							assertTrue(guid.equalsIgnoreCase("" + trialAppProperties.get("guid")));
							assertTrue(trialAppProperties.get("guid") instanceof java.util.UUID);
							}
			);
		
	}
	
	@Test
	public void phoneNumberTest() {
		String phone = "+1(510)555-1212";
		trialAppProperties.putProperty("phone", phone);
		assertTrue(phone.equals(TypeSafeAccessor.phoneNumberValue("phone")));
	}
	
	@Test
	public void errorPhoneNumberTest() {
		String errorPhone = "+1.510.555-123-456x255";
		trialAppProperties.putProperty("phone", errorPhone);
		assertTrue (errorPhone.equals(TypeSafeAccessor.valueOf("phone")));
		try {
			assertTrue (errorPhone.equals(TypeSafeAccessor.phoneNumberValue("phone")));
			assertFalse(true);
		} catch (IllegalArgumentException e) {
			assertFalse(false);
		}
	}
	
	@Test
	public void regionTest() {
		String[][] AWS = {
			{"aws_region_id", "us-west-1"},
			{"aws.Region_id", "us-west-2"},
		};
		
		Arrays.asList(AWS).forEach((kv) 
			-> {trialAppProperties.putProperty(kv[0], kv[1]);
				assertTrue(kv[1].equals("" + TypeSafeAccessor.awsRegionValue(kv[0])));
				assertTrue(kv[1].equalsIgnoreCase("" + trialAppProperties.get(kv[0])));
				assertTrue(kv[1].equals("" + trialAppProperties.get(kv[0])));
				}
			);
	}
	
	/** 
	 * test error Region definition
	 */
	@Test
	public void notRegionTest() {
		String[][] AWS = {
			{"aws_region_id", "us-east-1"},
			{"aws.Region_id", "us-west-3"},
		};
		
		Arrays.asList(AWS).forEach((kv) 
			-> {trialAppProperties.putProperty(kv[0], kv[1]);
			
				assertTrue(TypeSafeAccessor.valueOf(kv[0]) instanceof String);
				
				assertTrue(kv[1].equals("" + TypeSafeAccessor.valueOf(kv[0])));
				try {
					assertFalse(kv[1].equals("" + TypeSafeAccessor.awsRegionValue(kv[0])));
					assertFalse(true);
				} catch (IllegalArgumentException e) {
					assertFalse(false);
				}
				assertFalse(trialAppProperties.get(kv[0]) instanceof Region);
				assertTrue(kv[1].equalsIgnoreCase("" + trialAppProperties.get(kv[0])));
				}
			);
	}
	
	@Test
	public void regionsTest() {
		String[] awsRegions = {"aws.region",    "US_EAST_1"};
		
				trialAppProperties.putProperty(awsRegions[0], awsRegions[1]); 
				assertTrue(awsRegions[1].equals("" + TypeSafeAccessor.awsRegionsValue(awsRegions[0])));
				assertTrue(awsRegions[1].equalsIgnoreCase("" + trialAppProperties.get(awsRegions[0])));
				assertTrue(awsRegions[1].equals("" + trialAppProperties.get(awsRegions[0])));
	}
	
	/** 
	 * test error Regions definition
	 */
	@Test
	public void notRegionsTest() {
		String[] AWS = {"aws_region_id", "us_east_1"};
		
		trialAppProperties.putProperty(AWS[0], AWS[1]);
	
		assertTrue(TypeSafeAccessor.valueOf(AWS[0]) instanceof String);
		
		assertTrue(AWS[1].equals("" + TypeSafeAccessor.valueOf(AWS[0])));
		
		try {
			assertFalse(AWS[1].equals("" + TypeSafeAccessor.awsRegionsValue(AWS[0])));
			assertFalse(true);
		} catch (IllegalArgumentException e) {
			assertFalse(false);
		}
		assertFalse(trialAppProperties.get(AWS[0]) instanceof Region);
		assertTrue(AWS[1].equalsIgnoreCase("" + trialAppProperties.get(AWS[0])));
	}
	
}
