If you want to run project by one command only then 
there is special command file name as 
 - "build-n-test-n-run.bat" for windows environment
 - "run.sh" - is likes for linux environment


MORE DETAIL DESCRIPTION:

This project is build as usually maven project by command:

> mvn install

This command also automatically will be done all tests.

Resulting *.jar file will by lie in standard folder "target"



For copy target jar-file to root directory and add MANIFEST.MF to the jar
 
It's need execute command:

> jar cfm properties.jar  MANIFEST.MF -C target\classes\ .



After copy 'properties.jar' file to root folder you can 

RUN PROJECT by command:

> java -jar properties.jar classpath:jdbc.properties file://resources/aws.json http://jsonplaceholder.typicode.com/posts/1?test.json > output.txt

Note: This jar-file, named as properties.jar is used outer libraries that lies in 'lib' folder in root directory.


Importance Note:
In testing procedure is used url address "http://jsonplaceholder.typicode.com/posts/1?test.json" for get simple property json string.
This is temporary http address. So that in long using the test you need replace this address to another well known permanent address.
 